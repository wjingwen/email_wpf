# EmailApp #

## Overview
This project is a simple windows application to send emails. The project is done using C# and WPF under the .NET framework.

Emails and passwords are redacted for security reasons.

## Objective
Learn and practice C# .NET and WPF.

## Features
1. Enter or select from a dropdown menu a recipient
2. Enter a email subject(optional)
3. Attach a file(optional)
4. Enter a email body(optional)